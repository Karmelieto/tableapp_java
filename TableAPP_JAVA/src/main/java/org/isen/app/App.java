package org.isen.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


/**
 * JavaFX App
 */
public class App extends Application {


    @Override
    public void start(Stage primaryStage) throws IOException {


        String sceneFile = "/fxml/Connection.fxml";
        String title = "RotativeTable_Application";

        URL url = getClass().getResource(sceneFile);
        Parent root = FXMLLoader.load(url);

        InputStream iconStream = getClass().getResourceAsStream("/pictures/camera.png");
        Image image = new Image(iconStream);

        primaryStage.getIcons().add(image);
        primaryStage.setTitle(title);
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch();


    }

}