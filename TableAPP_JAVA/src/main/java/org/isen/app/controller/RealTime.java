package org.isen.app.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;
import org.isen.app.model.Save;

import java.io.IOException;

public class RealTime {

    private Save save;

    @FXML
    public VBox vBox;

    @FXML
    public void load(ActionEvent actionEvent) throws Exception {
        VBox root = FXMLLoader.load(getClass().getResource("/fxml/Selection.fxml"));
        vBox.getChildren().setAll(root);
    }

    @FXML
    public void back(ActionEvent actionEvent) throws IOException {
        VBox root = FXMLLoader.load(getClass().getResource("/fxml/Choose.fxml"));
        vBox.getChildren().setAll(root);
    }

  /*  @FXML
    public void saveData(ActionEvent actionEvent) throws Exception{


    }*/

}

