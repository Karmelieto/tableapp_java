package org.isen.app.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class Choose {

    @FXML
    public VBox vBox;

    public void initialize() {

    }

    @FXML
    public void realTimeMode(ActionEvent actionEvent) throws IOException {
        VBox root = FXMLLoader.load(getClass().getResource("/fxml/RealTime.fxml"));
        vBox.getChildren().setAll(root);
    }

    @FXML
    public void programmedMode(ActionEvent actionEvent) throws IOException {
        VBox root = FXMLLoader.load(getClass().getResource("/fxml/Programmed.fxml"));
        vBox.getChildren().setAll(root);
    }

    @FXML
    public void back(ActionEvent actionEvent) throws IOException {
        VBox root = FXMLLoader.load(getClass().getResource("/fxml/Actions.fxml"));
        vBox.getChildren().setAll(root);
    }
}

