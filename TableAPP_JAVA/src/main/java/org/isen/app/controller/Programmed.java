package org.isen.app.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class Programmed {

    @FXML
    public VBox vBox;

    @FXML
    public void load(ActionEvent actionEvent) throws Exception {
        VBox root = FXMLLoader.load(getClass().getResource("/fxml/Selection.fxml"));
        vBox.getChildren().setAll(root);
    }

    @FXML
    public void back(ActionEvent actionEvent) throws IOException {
        VBox root = FXMLLoader.load(getClass().getResource("/fxml/Choose.fxml"));
        vBox.getChildren().setAll(root);
    }
}

