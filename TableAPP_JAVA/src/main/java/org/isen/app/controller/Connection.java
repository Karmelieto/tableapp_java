package org.isen.app.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

public class Connection {

    @FXML
    public VBox vBox;

    @FXML
    public Button buttonRefresh;


    public ListView listView;

    public void initialize() {
        listView.getItems().add("Bluetooth ITEM 1");
        listView.getItems().add("Bluetooth ITEM 2");
        listView.getItems().add("Bluetooth ITEM 3");

        /*buttonRefresh.setStyle("-fx-background-image: url('/pictures/arrow-refresh-reload-icon-29.png');" +
                " -fx-background-size: 18 18;\n" +
                "    -fx-background-repeat: no-repeat;\n" +
                "    -fx-background-position:left; ");*/
    }

    public void refresh(ActionEvent actionEvent) {
        listView.getItems().clear();
        initialize();
    }

    @FXML
    public void connection(ActionEvent actionEvent) throws Exception {
        VBox root = FXMLLoader.load(getClass().getResource("/fxml/Actions.fxml"));
        vBox.getChildren().setAll(root);
    }

    public void changeListView() {
        int elt = listView.getItems().size() + 1;
        listView.getItems().add("Bluetooth ITEM " + elt);
    }
}

