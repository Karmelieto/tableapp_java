package org.isen.app.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

public class Actions {

    @FXML
    public VBox vBox;

    public void initialize() {

    }

    @FXML
    public void createCommand(ActionEvent actionEvent) throws Exception {
        VBox root = FXMLLoader.load(getClass().getResource("/fxml/Choose.fxml"));
        vBox.getChildren().setAll(root);
    }

}

