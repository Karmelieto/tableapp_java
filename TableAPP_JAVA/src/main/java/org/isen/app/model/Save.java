package org.isen.app.model;

public class Save {

    private int acceleration;
    private int nbrFoot;
    private int nbrCameras;
    private int nbrPictures;
    private int speed;
    private int pause;

    public Save() {
    }

    public Save(int acceleration, int nbrFoot, int nbrCameras, int nbrPictures, int speed, int pause, SaveType saveType) {
        this.acceleration = acceleration;
        this.nbrFoot = nbrFoot;
        this.nbrCameras = nbrCameras;
        this.nbrPictures = nbrPictures;
        this.speed = speed;
        this.pause = pause;
        this.saveType = saveType;
    }

    public int getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(int acceleration) {
        this.acceleration = acceleration;
    }

    public int getNbrFoot() {
        return nbrFoot;
    }

    public void setNbrFoot(int nbrFoot) {
        this.nbrFoot = nbrFoot;
    }

    public int getNbrCameras() {
        return nbrCameras;
    }

    public void setNbrCameras(int nbrCameras) {
        this.nbrCameras = nbrCameras;
    }

    public int getNbrPictures() {
        return nbrPictures;
    }

    public void setNbrPictures(int nbrPictures) {
        this.nbrPictures = nbrPictures;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getPause() {
        return pause;
    }

    public void setPause(int pause) {
        this.pause = pause;
    }

    public SaveType getSaveType() {
        return saveType;
    }

    public void setSaveType(SaveType saveType) {
        this.saveType = saveType;
    }

    private SaveType saveType;

}
