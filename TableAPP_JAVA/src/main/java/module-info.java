module org.isen {
    requires javafx.controls;
    requires javafx.fxml;
    requires kotlin.stdlib;
    requires com.google.gson;
    requires org.slf4j;
    exports org.isen.app;
    exports org.isen.app.controller;
}